# Utilisez une image de base (par exemple, node:18)
FROM node:18

# Définissez le répertoire de travail à l'intérieur du conteneur
WORKDIR /app

# Copiez package.json et package-lock.json dans le répertoire de travail
COPY package*.json ./

# Installez les dépendances
RUN npm install

# Copiez le reste du code de l'application dans le conteneur
COPY . .

# Exposez le port 3000
EXPOSE 3000

# Créez un dossier "dist" avec la version de production de l'application
RUN npm run build

# Démarrez le serveur en utilisant la version de production
CMD ["node", "dist/main.js"]
